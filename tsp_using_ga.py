import numpy as np
import random
from collections import defaultdict


def initialize_population(population_size, no_of_cities):
    initial_population = set()
    while len(initial_population) < population_size:
        initial_population.add(tuple(np.random.permutation(range(1, no_of_cities + 1))))
    return initial_population


def get_fitness(candidate, no_of_cities, distance_matrix):
    fitness = 0
    for i in range(no_of_cities):
        fitness += distance_matrix[
            candidate[(i - 1) % no_of_cities], candidate[i % no_of_cities]
        ]
    return fitness


def get_best_candidate(population, no_of_cities, distance_matrix):
    best = -1
    for candidate in population:
        if best == -1 or get_fitness(
            candidate, no_of_cities, distance_matrix
        ) < get_fitness(best, no_of_cities, distance_matrix):
            best = candidate
    return best


def select_candidate(population, no_of_cities, distance_matrix):
    # tournament selection
    tournament_size = 2
    random_population = random.sample(population, tournament_size)
    return get_best_candidate(random_population, no_of_cities, distance_matrix)


def get_edge_table(parent_1, parent_2):
    edge_table = defaultdict(set)
    for city_idx_in_parent_1, city in enumerate(parent_1):
        city_idx_in_parent_2 = parent_2.index(city)
        edge_table[city].add(parent_1[(city_idx_in_parent_1 - 1) % len(parent_1)])
        edge_table[city].add(parent_1[(city_idx_in_parent_1 + 1) % len(parent_1)])
        if parent_2[(city_idx_in_parent_2 - 1) % len(parent_2)] in edge_table[city]:
            edge_table[city] = {
                -c if c == parent_2[(city_idx_in_parent_2 - 1) % len(parent_2)] else c
                for c in edge_table[city]
            }
        else:
            edge_table[city].add(parent_2[(city_idx_in_parent_2 - 1) % len(parent_2)])
        if parent_2[(city_idx_in_parent_2 + 1) % len(parent_2)] in edge_table[city]:
            edge_table[city] = {
                -c if c == parent_2[(city_idx_in_parent_2 + 1) % len(parent_2)] else c
                for c in edge_table[city]
            }
        else:
            edge_table[city].add(parent_2[(city_idx_in_parent_2 + 1) % len(parent_2)])
    return edge_table


def remove_included_city_from_edge_table(edge_table, city):
    for c in edge_table:
        edge_table[c].discard(city)
        edge_table[c].discard(-city)
    return edge_table


def get_offspring(parent_1, parent_2, no_of_cities):
    # enhanced edge recombination operator
    edge_table = get_edge_table(parent_1, parent_2)
    offspring = []
    city = np.random.choice((parent_1[0], parent_2[0]))
    offspring.append(city)
    while len(offspring) < len(parent_1):
        edge_table = remove_included_city_from_edge_table(edge_table, city)
        common_edge_city = 0
        if len(edge_table[city]) > 0:
            for c in edge_table[city]:
                if c < 0:
                    common_edge_city = c
                    break
            if common_edge_city != 0:
                city = abs(common_edge_city)
                offspring.append(city)
            else:
                adj_cities_with_lst_len = [
                    (c, len(edge_table[c])) for c in edge_table[city]
                ]
                min_len = min([ac[1] for ac in adj_cities_with_lst_len])
                adj_cities_with_min_lst_len = [
                    c for c, length in adj_cities_with_lst_len if length == min_len
                ]
                if len(adj_cities_with_min_lst_len) > 1:
                    for c in adj_cities_with_min_lst_len:
                        city = c
                        if any([x < 0 for x in edge_table[c]]):
                            city = c
                            break
                else:
                    city = adj_cities_with_min_lst_len[0]
                offspring.append(city)
        else:
            cities = {x + 1 for x in range(no_of_cities)}
            offspring.append(random.sample(cities.difference(offspring), k=1)[0])
    return tuple(offspring)


def mutate_offspring(offspring, no_of_cities, prob=0.001):
    if np.random.choice([True, False], p=[prob, 1 - prob]):
        rand_pos_1, rand_pos_2 = random.sample(range(no_of_cities), 2)
        mutated_offspring = list(offspring)
        mutated_offspring[rand_pos_1], mutated_offspring[rand_pos_2] = (
            offspring[rand_pos_2],
            offspring[rand_pos_1],
        )
        return tuple(mutated_offspring)
    return offspring


def get_mating_pool(population, mating_pool_size, no_of_cities, distance_matrix):
    mating_pool = set()
    while len(mating_pool) < mating_pool_size:
        # selection
        candidate = select_candidate(population, no_of_cities, distance_matrix)
        mating_pool.add(candidate)
    return mating_pool


def tsp_using_ga(no_of_cities, distance_matrix):
    population_size = 100
    population = initialize_population(population_size, no_of_cities)
    best_candidate = get_best_candidate(population, no_of_cities, distance_matrix)
    mating_pool_size = population_size // 2
    no_of_fitness_values_to_track = 100
    fitness_values = [get_fitness(best_candidate, no_of_cities, distance_matrix)]
    threshold = 0.01
    iterations = 0
    while not (
        len(fitness_values) == no_of_fitness_values_to_track
        and (max(fitness_values) - min(fitness_values) < threshold)
    ):
        # selection
        mating_pool = get_mating_pool(
            population, mating_pool_size, no_of_cities, distance_matrix
        )
        population = set()
        population.update(mating_pool)
        while len(population) < population_size:
            parent_1 = random.sample(mating_pool, k=1)[0]  # returns a list
            parent_2 = random.sample(mating_pool, k=1)[0]  # returns a list
            while parent_1 == parent_2:
                parent_2 = random.sample(mating_pool, k=1)[0]
            # crossover
            offspring = get_offspring(parent_1, parent_2, no_of_cities)
            # mutation
            offspring = mutate_offspring(offspring, no_of_cities, prob=0.01)
            population.add(offspring)
        best_candidate = get_best_candidate(population, no_of_cities, distance_matrix)
        best_candidate_fitness = get_fitness(
            best_candidate, no_of_cities, distance_matrix
        )
        if len(fitness_values) >= no_of_fitness_values_to_track:
            fitness_values.pop(0)
        fitness_values.append(best_candidate_fitness)
        iterations += 1
        print("---")
        print(best_candidate)
        print(best_candidate_fitness)
    print("===")
    print("No of iterations | ", iterations)
    print("Best candidate found | ", best_candidate)
    print("Best distance found | ", best_candidate_fitness)


def process_file_with_distances(file_path):
    with open(file_path, "r") as f:
        f_content = f.read()
    f_list = f_content.strip().split("\n")
    no_of_cities = int(f_list[0])
    distance_matrix = {}
    for line in f_list[1:]:
        line_as_lst = list(map(int, line.strip().split()))
        distance_matrix[line_as_lst[0] + 1, line_as_lst[1] + 1] = line_as_lst[2]
        distance_matrix[line_as_lst[1] + 1, line_as_lst[0] + 1] = line_as_lst[2]
    for i in range(no_of_cities):
        distance_matrix[i + 1, i + 1] = 0
    return no_of_cities, distance_matrix


def main():
    file_path = "testdata2.txt"
    no_of_cities, distance_matrix = process_file_with_distances(file_path)
    tsp_using_ga(no_of_cities, distance_matrix)


if __name__ == "__main__":
    main()
