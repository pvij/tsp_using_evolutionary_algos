This is an attempt to solve the Travelling Salesman Problem (TSP) using evolutionary computing algorithms. It has been assumed that the graph is complete (i.e. every two nodes are connected by an edge). 

The datasets used are testdata1.txt (http://www.cse.iitm.ac.in/~vplab/courses/DSA/testdata1.txt) and testdata2.txt (http://www.cse.iitm.ac.in/~vplab/courses/DSA/testdata2.txt). They can be changed in the file itself. Both the files need to be in the same directory as the code.

The following algorithms have been used: -

1. **Genetic Algorithm**

* Permutation encoding has been used to represent a candidate solution.
* Tournament selection has been used for creating the mating pool.
* Enhanced Edge Recombination operator has been used for performing crossover (so that the resultant offspring is a valid candidate). In the data sets used, for a graph with n cities, the labeling is 0, 1, ..., n-1. In the code, the labeling is converted to 1, 2, ..., n to implement Enhanced Edge Recombination Operator (E.g., for two parents (1, 2, 3, 4, 5, 6) and (4, 2, 3, 6, 1, 5), the edge list for node 1 is (2, -6, 5). Neighbours for parent 1 are 2 and 6 whereas in parent 2 are 6 and 5. Since the edge 1-6 is present in both parents, a minus sign is appended to the city in the edge list. Since a minus sign cannot be appended to 0 in python, the new labeling was used.
* The code for solving TSP using genetic algorithm is in `tsp_using_ga.py`.
